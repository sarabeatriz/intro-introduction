# Introduction to the laboratory and the programs we will be using


![main1.png](images/main1.png)

This is the first of a series of lab experiences that were developed as part of the NSF-sponsored project "Development of engaging and readily transferable laboratory experiences for the introductory programming course" (DUE-1245744). The main goal of these lab experiences is that the students of introductory programming courses will practice the concepts and skills learned in class and obtain attractive products that will motivate them to further explore the endless opportunities of fun and applicability of Computer Science.

Each of the laboratory experiences uses libraries and reusable code developed by professors and students of the Computer Science program at the University of Puerto Rico, Rí­o Piedras campus ([http://ccom.uprrp.edu](http://ccom.uprrp.edu)). When each laboratory experience is completed, the student will not only reinforce the concepts learned in class, but will also get a glimpse of the products that he or she may achieve with a bit more knowledge and experience in computing.

In this first laboratory experience you will learn the workflow for the the upcoming meetings, how to access the files that will be used, and how to turn in your deliverables. You will also learn how to manage the basic elements of Qt, the platform that will allow us to develop and execute projects in C++.

## Objectives:

1. Get to know the laboratory experiences prepared and the general expectations of the laboratory sessions.

2. Practice the use of *Git* and *Bitbucket* to download and store the necessary files for each laboratory experience.

3. Practice how to compile, fix errors, and execute a program using Qt.

---

## Services and programs we will use:

### Electronic book

The instructions for the laboratory experience for this semester are contained in the electronic book you can access at [http://eip.ccom.uprrp.edu/_book-en/](http://eip.ccom.uprrp.edu/_book-en/).

We will publish the name of each week’s lab experience in our course’s Moodle page.

This is a preliminary version of the book and therefore may contain errors. We would appreciate it if you would tell us if you find any errors or if you have any suggestion that could help clarify the instructions. Please notify us at: eip.uprrp@upr.edu. 

### Bitbucket and Git

*Bitbucket* is a repository or deposit for digital files that can be accessed online and allows the development of group projects in an ordered and simple manner. The files for the Introduction to Programming Laboratory will be stored in this site and can be downloaded to your personal computers using *Git*.

*Git*  is an open source version control system that is primarily used for software development. The EIP team used Git during software development of the lab experiences. You as a student probably will not need to use of Git's version control capabilities for the coding assignments in the lab experiences. However, you will use Git to download the lab experience's source code files at the beginning of each session, e.g. using `git clone url`. You can obtain *Git* at [http://git-scm.com/](http://git-scm.com/).

### Terminal and Linux

The programming exercises developed by EIP can be completed in any of the three major operating systems: Linux, OSX, and Windows. Regardless of your preferred operating system, we have found that mastering the command line always helps students complete the programming exercises more comfortably. The following are some useful commands for the OSX and Linux terminal. 

---

|    **Command**     |                      **Action**                     |
| ------------------ | --------------------------------------------------- |
| ls                 | shows a list of the files in the directory      |
| mv name1 name2 | moves the contents of name1 to file name2 (changes the name of the file)       |
| cp name1 name2 | copies the contents of name1 to file name2       |
| rm name          | deletes file                                    |
| mkdir name       | creates a directory name within the current directory |
| cd ..              | moves to the previous directory                      |
| cd ~               | moves to the home directory                        |
| cd name          | moves to the name directory (within the current one)    |
| upper arrow | repeats the previous command                          |

           
**Figure 1.** Basic Unix commands.

---


### Qt

Qt is a cross-platform application framework that is widely used by many organizations for developing multi-platform applications. C++ is the main programming language of Qt. The QT framework works in, and can create versions of, applications for different platforms (desktop, mobile platforms, among others). The Qt project provides an integrated development environment (IDE), called Qt Creator. We urge you to install Qt and Qt Creator on your personal computer and explore the options these applications provide.  We also encourage you to get inspired by exploring the diversity of applications developed by the Qt community at [https://showroom.qt.io/](https://showroom.qt.io/) .


### Virtual Machine

The EIP project distributes all their products (electronic book, source code, etc) as a virtual machine available at [http://eip.ccom.uprrp.edu/vms/](http://eip.ccom.uprrp.edu/vms/). The compressed file contains a VMDK file that is playable in VMware Workstation or VirtualBox. 

We encourage you to use the virtual machine since it comes preinstalled with Qt, the online book and source code directories for the various lab experiences.  Besides, the source code has been tested with the Qt version that is preinstalled. If you install another version of Qt, you may run into problems with some of the programs.


## How to obtain Qt

If you decide to install the Qt application natively on your computer, you can download it from the Qt project page at [http://qt-project.org/](http://qt-project.org/). Our project developed some videos that instruct you on how to install Qt on the various operating systems at: [http://youtu.be/6eY5VSPYZCw](http://youtu.be/6eY5VSPYZCw) (Linux), [http://youtu.be/CImDCSxi7Wc](http://youtu.be/CImDCSxi7Wc) (Windows), [http://youtu.be/_zq-pSw3Ox4](http://youtu.be/_zq-pSw3Ox4) (Mac).

---

---

## Using Qt

In this laboratory experience we will learn how to use some of the basic functionalities of Qt Creator. We will be using Qt Creator mostly for editing and debugging our programs. However, Qt Creator also includes Qt Designer, the Qt tool for GUI layout/design which was used for designing the graphical interfaces for the laboratory experiences. Learning to use this tool is not part of this course, but you can learn to use it on your own. In [https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk](https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk)  you can find a presentation, prepared by the student Jonathan Vélez, that shows the basics of using Qt Creator and Qt Designer for designing graphical interfaces.



### Projects in C++

Each C++ project is composed of various types of files. In Qt you will have files of type *source, header, and form*.

- **Source files:** These files have a `.cpp` extension (C plus plus) and contain the C++ source code for your program. Among these files you will find `main.cpp`;  this is the file that typically contains the main function the preprocessor will look for to start your program. Another source file you will find in projects created with Qt is the `mainwindow.cpp` file; this file is created by Qt and contains the implementation of the functions that control code associated with the main GUI window designed with the design option (for example the functions that appear below "Private slots").

- **Header files:** These files have a `.h` extension and contain declarations of the functions that are used in the program. 

- **Form files:** These files have a `.ui` extension (user interface) and contains a description of the controls and widgets included in the application's GUI.


---

---


## Laboratory session

In this laboratory experience you will practice the use of the some of the programs you will use during the semester. You will use the programs that are installed in the virtual machine.


### Exercise 0: Create a directory for the laboratory files

Use the terminal and the command `mkdir` to create a directory `Documents/eip` for the laboratory files.


### Exercise 1: Starting your new project


#### Instructions

1. To start a C++ project, press the `New Project` button or go to the Qt Creator main menu and in `File` select `New File or Project`. A window similar to the one in Figure 2 will appear. Select `Non-Qt Project`, `Plain C++ Project` and press `Choose`.

    ---

    ![figure2.png](images/figure2.png)

    **Figure 2.** Start C++ project without graphical applications.

    ---

2. Write the name of the project, select the directory where you would like to save it, press `Next` in that window and the following one, and then `Finish` in the next one.  
    
    This process will create a new Qt project with the skeleton for a basic C++ program that only displays "Hello World!". Before continuing, select `Projects` in the vertical menu to the left. A window called `Build Settings` should appear. In this window, make sure the `Shadow build` checkbox is NOT selected, as shown in Figure 3.
    
    ---

    ![figure3.png](images/figure3.png)

    **Figure 3.** The `Shadow build` option is not selected.
    
    ---

3. Return to the window where you can edit the program by selecting `Edit` in the left menu and double clicking `Sources` and then `main.cpp`. Press the green arrow on the left menu to execute the program. The results for the program will be displayed in the terminal window. If there were any errors, these would appear in the `Issues` window in Qt Creator.

4. Change the contents of `main.cpp` to the following:

    ```cpp
    #include <iostream>
    using namespace std;
        
    int main()
    {
        cout << endl << "I like the programming laboratory." << endl;
        return 0;
    }

    ```
    ---

1. Press the green button in the left menu to compile and execute the program. A window will appear that offers the option to store the changes. Press `Save all`. When executed, if you did not make any mistakes, the program will display "I like the programming laboratory." in the terminal window.


### Exercise 2: Download projects from Bitbucket

The files for this laboratory experience are stored in Bitbucket. In each laboratory session, you will download the Bitbucket folder that contains the files for that laboratory experience and you will save them in the `Documents/eip` directory you created in Exercise 0. To download the corresponding folder for this laboratory experience, open the terminal, use the Linux commands to change directory to `Documents/eip` directory and execute the command `git clone https://bitbucket.org/eip-uprrp/intro-introduction.git`. Go to the `Documents/eip` directory and verify that it contains the `intro-introduction` folder.


### Exercise 3: Open project already created, compile and run

In this exercise you will practice how to compile, fix errors, and execute a program using Qt Creator.


#### Instructions

1. First delete the files that were created by Qt during the building process and close the files from the previous project. To do this, in the Qt Creator main menu go to `Build` and select `Clean all`; then go to `File` and select `Close all projects and editors`.

2. Load the `QtCreator` project called `Practice` by double clicking the file `Practice.pro` in the `Documents/eip/introduction` directory on your computer. In the window that appears, press `Configure Project`.

    Each time you load or start a new project, make sure that `Shadow build` is not selected: in the menu to the left, select `Projects`, then in `Build Settings` verify that the checkbox for `Shadow build` is not selected, as we saw in Figure 3.

3. As you saw previously, Qt allows you to build and execute a program by pressing the green arrow that appears in the left column. Press the arrow and notice that you obtain a window of "Issues" that occurred when building. The list that appears will show you information that allows you to find and fix the errors.

4. Select the `main.cpp` file in the `Sources` directory so you can find and fix the errors.

    Fix all of the errors and press the green arrow once again to build and execute the program. Once you fix the errors, the program should open a window `Application Output` and display `Exit: 1`.

5. As mentioned earlier, during the compilation and execution process, Qt creates various files we should delete after finishing the program. To do this, in the `Build` option of the Qt Creator menu, select `Clean All`.


### Exercise 4: Deliverables

During each laboratory experience, each student will hand in some of the results of their work. These deliveries will be done in the "Deliverables" section that appears on Moodle. Today each student will practice handing in their work individually.


#### Instructions

1. Open the "Deliverables" link on Moodle and hand in the `main.cpp` file. Remember to use good programming techniques, including the name of the programmers as a comment at the beginning of the program.
